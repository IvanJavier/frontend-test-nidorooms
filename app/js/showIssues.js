function getIssues(){
    $.ajax({
        method: "GET",
        url: "https://api.github.com/repos/AlvaroCarrillo/test/issues",
        dataType: "JSON"
    })
    .done(function(response){
        $("#dataTableListIssues").show();
        response.forEach(element => {
            $("#dataIssue").append("<tr><td>"+element.id+"</td>"
            +"<td>"+element.author_association+"</td><td>"+element.body+"</td>"
            +"<td>"+element.number+"</td><td>"+element.state+"</td><td>"+element.title+"</td>"
            +"<td>"+element.user.login+"</td><td>"+element.created_at+"</td></tr>");
        });
        console.log(response);
    })
    .fail(function(response){
        alert("error");
    })
    .always(function(response){
        console.log('complete');
    });
}
