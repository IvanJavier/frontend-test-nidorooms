function createIssue(){

    var data = {
        "title" : $("#title").val(),
        "body" : $("#body").val(),     
    };

    var headers = {
        "Authorization" : `Token 213b7352fad91235765c1566e2414cd710adb7b1`
    }

    $.ajax({
        method: "POST",
        url: "https://api.github.com/repos/AlvaroCarrillo/test/issues",
        data: JSON.stringify(data),
        dataType: "JSON",
        headers: headers,
        success: function (response) {
            alert("Issue was created!");
        },
        error: function (xhr) {
            alert("error in: " +xhr);
            console.log(xhr);
        }
    });
}

$("#showInput").click(function () { 
    $("#showCreateIssue").show();
});